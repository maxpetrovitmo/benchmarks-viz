import pandas
import matplotlib.pyplot as plt
import numpy

dateparse = lambda x: pandas.datetime.strptime(x, '%Y-%m-%d %H:%M:%S')
data = pandas.read_csv('rawrawdata.csv', header=0, parse_dates=['time'], date_parser=dateparse).drop('id', 1)
print(data)

plt.figure(figsize=(20, 10))
plt.title('Throughput')
plt.xlabel('Time')
plt.ylabel('Count')
plt.plot(data['time'],data['count'],'.-g')
plt.show()