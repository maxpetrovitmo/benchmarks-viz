import redis
import pandas

r = redis.Redis(host='192.168.92.73')

times = []

campaigns = r.smembers("campaigns")
for campaign in campaigns:
    windows_key = r.hget(campaign, 'windows')
    window_count = r.llen(windows_key)                # getting list size
    windows = r.lrange(windows_key, 0, window_count)  # getting list

    for window_time in windows:
        window_key = r.hget(campaign, window_time)
        seen = r.hget(window_key, "time_updated")
        ended = int(seen)
        started = int(window_time)
        latency = ended - started
        times.append((started, ended, latency))

times.sort(key=lambda x: x[0])


data = pandas.DataFrame(times, columns=['started', 'end', 'latency'])
print(data)
data.to_csv("input_data.csv")
