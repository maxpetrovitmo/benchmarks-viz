import datetime
import redis
import pandas
import numpy
from functional import seq
import matplotlib.pyplot as plt


r = redis.Redis(host='192.168.92.73')

times = []

campaigns = r.smembers("campaigns")
for campaign in campaigns:
    windows_key = r.hget(campaign, 'windows')
    window_count = r.llen(windows_key)                # getting list size
    windows = r.lrange(windows_key, 0, window_count)  # getting list
    # print(campaign, windows_key, windows)

    for window_time in windows:
        window_key = r.hget(campaign, window_time)
        seen = r.hget(window_key, "time_updated")
        ended = int(seen)
        started = int(window_time)
        times.append(( ended, ended - started))

times.sort(key=lambda x: x[0])


data = pandas.DataFrame(times, columns=['end', 'delta'])
# data.to_csv("rawrawdata.csv")


interval = 10000  # 10 sec

d = seq(times)\
    .group_by(lambda x: int(x[0] / interval))\
    .map(lambda g: (datetime.datetime.fromtimestamp(g[0]), len(g[1])))\
    .sorted(lambda g:g[0])
d.for_each(print)
print(d)



#  Throughput

x = d.map(lambda x: x[0]).to_list()
y = d.map(lambda x: x[1]).to_list()

plt.figure(figsize=(20, 10))
plt.title('Throughput. Mean = {} Count = {} Groups = {}'.format(numpy.mean(y), sum(y), len(y)))
plt.xlabel('Time')
plt.ylabel('Count')
plt.plot(x, y,'.-g')
plt.show()


# Rolling mean Throughput

data = pandas.DataFrame([(x[i],y[i]) for i in range(len(x))], columns=['time', 'count'])
print(data)
# data.to_csv("rawdata.csv")

rolling_mean = pandas.rolling_mean(data['count'], 10)
print(rolling_mean)

plt.figure(figsize=(20, 10))
plt.title('Rolling mean throughput. Mean = {} Count = {} Groups = {}'.format(numpy.mean(y), sum(y), len(y)))
plt.xlabel('Time')
plt.ylabel('Count')
plt.plot(rolling_mean,'.-g')
plt.show()

# rolling_mean.to_csv("data.csv")

# pandas.read_csv('csvfile.txt', index_col=False, header=0)



